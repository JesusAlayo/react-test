import React, { Component } from 'react';

// Ma terial
import Icon from '@material-ui/core/Icon';

// Estilos
import './Equipo.css';

class Equipo extends Component {

  state = {
    fav: { value: 'star'}
  }

  setFav () {
      console.log("Favorito :D");

      const fav = { ...this.state.fav };
      fav.value = fav.value === 'star'? "star_selected" : "star";
      this.setState({fav});
  }

  componentDidMount () {
      const  localFav = localStorage.getItem(this.props.nombre);
      if (localFav) {
          this.setState({ fav: JSON.parse(localFav) });
      }
  }

  componentDidUpdate () {
      console.log(this.props);
      console.log(this.state.fav);

      localStorage.setItem(this.props.nombre, JSON.stringify(this.state.fav ))
  }

  render() {
    return (
      <div>
        <img className="team-logo" src={this.props.logo} alt={this.props.nombre} />
          <Icon onClick={ this.setFav.bind(this) } className={ this.state.fav.value }>star_rate</Icon>
      </div>
    );
  }
}

export default Equipo;
